import os


def read_text(filename: str) -> str:
    result = 'foo bar'
    return result


def tokenize(input_text: str) -> dict:
    result = {0: 'foo', 1: 'bar'}
    return result
